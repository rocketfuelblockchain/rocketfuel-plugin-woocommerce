# RocketFuel - RocketFuel Payment Method for WordPress 5.x
RocketFuel Payment Method 1.2.3 for WordPress 5.x

# Install


* Download the module from RF Bitbucket repository and zip it (zip archive).
* Go to your shop admin panel.
* Go to "Plugins" -> "Add Plugins".
* Click on "Upload Plugin" button and browse and select plugin zip file.
* After installation activate plugin.
* Enter "Merchant ID" (provided in RocketFuel merchant UI for registered merchants) in the bottom of General settings.
* Enter "Public signature" (provided in RocketFuel).
* Copy a RocketFuel callback URL and save settings
* Go to your RocketFuel merchant account
* Click "Edit" in the bottom left corner. A window will pop up.
* Paste callback URL and click "Save".
* Choose https://iframe-stage1.rocketdemo.net for stage1 RocketFuel environment in RocketFuel URL field.


