#!/bin/bash
echo "build rocketfuel plugin - zip archive"
cd ..
mkdir -p dist/rocketfuel
cp -r . dist/rocketfuel
cd dist
zip -r rocketfuel-plugin-woocommerce.zip rocketfuel/ -x *.git* *.ci-* *bitbucket-* *dist*
