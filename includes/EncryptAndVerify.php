<?php


class EncryptAndVerify
{

    static $signature_algorithm = 'SHA256';

    /**
     * @param WP_REST_Request $request
     * @param Rocketfuel_order $order
     * @return bool
     */
    public static function verifyPaymentResponse(WP_REST_Request $request,  $order_id)
    {
        $order=new Rocketfuel_order($order_id);

        $pub_key_path = dirname(__FILE__) . '/rf.pub';

        if (!$request->get_param('signature') || !file_exists($pub_key_path)) {
            return false;
        }

        $public = openssl_get_publickey(file_get_contents($pub_key_path));

        $data = json_encode(EncryptAndVerify::signAndSerializePayload($order->get_order()));

        return openssl_verify($data, base64_decode($request->get_param('signature')), $public, static::$signature_algorithm);
    }

    /**
     * @param $order
     * @return array
     */
    public static function signAndSerializePayload($order = [])
    {
        $sortedPayload = static::sortPayload($order);
        return $sortedPayload;
    }

    /**
     * @param $payload
     * @return array
     */
    public static function sortPayload($payload)
    {
        $sorted = [];
        if (is_object($payload))
            $payload = (array)$payload;
        $keys = array_keys($payload);
        sort($keys);

        foreach ($keys as $key)
            $sorted[$key] = is_array($payload[$key]) ? static::sortPayload($payload[$key]) : (string)$payload[$key];

        return $sorted;
    }

    /**
     * @param $payload
     * @return string
     */
    public static function getSignature($payload)
    {
        return base64_encode(hash_hmac('sha256', $payload, static::$pub_key, true));
    }

    /**
     * get crypt part for cart
     * prepare to send into iframe
     *
     * @param $amount
     * @param $order_id
     * @return string
     */
    public static function getEncrypted($amount, $order_id)
    {
        $to_crypt = json_encode([
            'amount' => $amount,
            'merchant_id' => get_option('rocketfuel_merchant_id'),
            'order' => $order_id
        ]);


        $out = '';

        $cert = get_option('rocketfuel_public_key');

        $public_key = openssl_pkey_get_public($cert);

        $key_lenght = openssl_pkey_get_details($public_key);

        $part_len = $key_lenght['bits'] / 8 - 11;
        $parts = str_split($to_crypt, $part_len);
        foreach ($parts as $part) {
            $encrypted_temp = '';
            openssl_public_encrypt($part, $encrypted_temp, $public_key,OPENSSL_PKCS1_OAEP_PADDING);
            $out .=  $encrypted_temp;
        }

        return base64_encode($out);
    }
}
