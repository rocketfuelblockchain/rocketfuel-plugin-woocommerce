<?php

class Rocket_curl{
    private $rocket_curl;
    private $URL;

    public function __construct()
    {
        $this->URL = get_option('rocketfuel_url');
    }

    public function get_stocks($access_token){
        $this->init();
        $this->set_settings('stock-market/my', $access_token);
        $data = $this->send_request();
        $this->close();
        return $data;
    }

    public function pay($pay_data){
        $this->init();
        $this->set_Post_settings($pay_data);
        $this->set_settings('purchase', $_REQUEST['payData']['access']);
        $data = $this->send_request();
        $this->close();
        return $data;
    }

    public function login($data){
        $this->init();
        $this->set_Post_settings($data);
        $this->set_settings('auth/login');
        $data = $this->send_request();
        $this->close();
        return $data;
    }

    private function set_Post_settings($data)
    {
        curl_setopt($this->rocket_curl, CURLOPT_POST, 1);
        curl_setopt($this->rocket_curl, CURLOPT_POSTFIELDS, $data);
    }

    private function set_settings($method_address, $key = ''){
        $headers[] = 'Content-Type: application/json';
        if($key != '') $headers[] = 'Authorization: Bearer '.$key;
        curl_setopt($this->rocket_curl, CURLOPT_URL, $this->URL.$method_address);
        curl_setopt($this->rocket_curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($this->rocket_curl, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($rocket_curl, CURLOPT_HEADER, true);
    }

    private function send_request(){
        $server_output = curl_exec ($this->rocket_curl);
        $request_info = curl_getinfo($this->rocket_curl);

        $result['info'] = $request_info['http_code'];
        $result['data'] = json_decode($server_output, true);
        return $result;
    }

    private function init(){
        $this->rocket_curl = curl_init();
    }

    private function close(){
        curl_close ($this->rocket_curl);
    }
}
