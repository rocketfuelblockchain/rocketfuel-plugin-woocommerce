<?php

use Automattic\WooCommerce\Admin\Overrides\Order;

class Rocketfuel_order
{
    private $order;
    private $cart;
    private $amount;

    public function __construct($id = null)
    {
        if (!empty($id)) $this->get_order($id);
    }

    /**
     * Get WC order by ID
     * has additional "raw" argument (default=false)
     *
     * @param null $id
     * @param bool $raw
     * @return array|WC_Order
     */
    public function get_order($id = null, $raw = false)
    {

        if (!empty($id)) $this->order = new WC_Order($id);

        if (!$raw) return $this->orderToApiFormat($this->order);

        return $this->order;
    }

    /**
     * @return mixed
     */
    public function get_id()
    {
        return $this->order->get_id();
    }

    /**
     * function that require for proper response format preparation
     *
     * @param $order
     * @return array
     */
    private function orderToApiFormat($order)
    {
        $new_order = [
            'amount' => $order->get_total(),
            'cart' => [],
            'currency' => $order->get_currency(),
            'order' => $order->get_id(),
            'encrypted' => EncryptAndVerify::getEncrypted($order->get_total(), $order->get_id())
        ];

        foreach ($order->get_items() as $itm_id => $item) {
            $wc_product = wc_get_product($item->get_product_id());
            $new_order['cart'][] = [
                'id' => $item->get_product_id(),
                'name' => $item->get_name(),
                'price' => $wc_product->price,
                'quantity' => $item->get_quantity()
            ];
        }

        $shipping_total = $order->get_shipping_total();
        if($shipping_total){
            $new_order['cart'][] = [
                'id' => '',
                'name' => 'Shipping: '.$order->get_shipping_method(),
                'price' => $shipping_total,
                'quantity' => 1,
            ];
        }

        return $new_order;
    }

    public function create_order($address)
    {
        $this->order = wc_create_order();
        $this->add_cart_in_order();
        $this->set_address_in_order($address);
        $this->order->calculate_totals();
        return $this->order->save();
    }

    private function add_cart_in_order()
    {
        foreach ($this->cart as $item) {
            $this->order->add_product(wc_get_product($item['data']->get_id()), $item['quantity']);
        }
    }

    private function set_address_in_order($address)
    {
        $this->order->set_address($address, 'billing');
        $this->order->set_address($address, 'shipping');
    }

    public function get_client_cart()
    {
        $result = array(
            'cart' => $this->client_cart(),
            //for testing price
            'amount' => $this->amount,
            'currency' => get_woocommerce_currency()
        );
        return $result;
    }

    private function client_cart()
    {
        foreach ($this->cart as $item) {
            $wc_product = wc_get_product($item['data']->get_id());
            for ($i = 1; $i <= $item['quantity']; $i++) {
                $cart[] = array(
                    'id' => strval($item['data']->get_id()),
                    'price' => $wc_product->get_price(),
                    'name' => $wc_product->get_name()
                );
                $this->amount += $wc_product->get_price();
            }
        }
        return $cart;
    }

    public function order_complete($transaction_id = '')
    {
        $this->order->payment_complete($transaction_id);
        return $this->order->get_checkout_payment_url(true);
    }
}
