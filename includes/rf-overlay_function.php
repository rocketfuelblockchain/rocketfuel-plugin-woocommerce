<?php
//    add_action( 'woocommerce_review_order_before_payment', 'rf_overlay_button' );

add_action('woocommerce_thankyou', 'js_variables', 100);
add_action('woocommerce_thankyou', 'rocketFuel_register_assets', 100);
add_action('woocommerce_thankyou', 'rf_overlay_button');
//    add_action( 'woocommerce_review_order_before_submit', 'rf_rocketfuel_block' );


function include_scripts(){
    add_action('wp_enqueue_scripts', 'rocketFuel_register_assets', 100);
}

function rf_overlay_button()
{
    ?>
    <button type="button" onclick="openOverLayBlock()"><strong>rocketfuel</strong></button>
    <?php
}

//Подключение скриптов и стилей
function rocketFuel_register_assets()
{
    wp_enqueue_script("jquery");
    wp_register_script('rocketFuel_js_script', plugins_url('assets/js/rocketFuel.js', dirname(__FILE__)), false, true);
    wp_enqueue_script('rocketFuel_js_script', plugins_url('assets/js/rocketFuel.js', dirname(__FILE__)), false, true);
    wp_enqueue_style('rocketfuel_style', plugins_url('assets/css/rf-rocketfuel-style.css', dirname(__FILE__)), false, true);
}

function js_variables()
{
    $variables = array(
        'ajax_url' => admin_url('admin-ajax.php'),
        'iframe_url' => get_option('rocketfuel_iframe_url', 'https://iframe-stage1.rocketdemo.net'),
    );
    if ($_GET['key']) {
        $variables['orderId'] = wc_get_order_id_by_order_key($_GET['key']);
    }
    echo
        '<script type="text/javascript">window.rocketFuelOptions = ' . json_encode($variables) . ';</script>';
}
