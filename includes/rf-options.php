<?php
//'http://'.$_SERVER['HTTP_HOST']
//add_action('admin_menu', 'rocketfuel_init_menu');
add_action('admin_init', 'rocketfuel_settings');

const IFRAMES = [
    'https://dev-iframe.rocketdemo.net',
    'https://qa-iframe.rocketdemo.net',
    'https://preprod-iframe.rocketdemo.net',
    'https://iframe-stage1.rocketdemo.net',
    'https://iframe.rocketdemo.net',
    'https://iframe-stage2.rocketdemo.net',
    'https://iframe.rocketfuelblockchain.com'
];

function rocketfuel_init_menu()
{
    add_options_page('RocketFuel', 'RocketFuel', 'manage_options', 'rocketfuel_settings', 'rocketfuel_settings');
}

function rocketfuel_settings()
{
    try {
        merchant_id();
        rocketfuel_url();
        address_for_rocketfuel_callback();
        public_key();
    } catch (Exception $e) {
        echo $e;
    }
}

/**
 * display rocketfuel public client key for crypt data in cart
 */
function public_key()
{
	register_setting('general', 'rocketfuel_public_key', 'sanitize_textarea_field');

	add_settings_field(
		'rocketfuel_public_key',
		'<label for="rocketfuel_public_key">RocketFuel public key</label>',
		'public_key_field_html',
		'general'
	);
}

function public_key_field_html()
{
	$value = get_option('rocketfuel_public_key', '');
	printf('<textarea id="rocketfuel_public_key" name="rocketfuel_public_key" rows="15" cols="80">'.$value.'</textarea>');
}

/**
 *  display rocketfuel input in settings form
 */
function merchant_id()
{

    register_setting('general', 'rocketfuel_merchant_id', 'sanitize_text_field');

    add_settings_field(
        'rocketfuel_merchant_id',
        '<label for="rocketfuel_merchant_id">merchant id</label>',
        'merchant_id_field_html',
        'general'
    );
}

function merchant_id_field_html()
{
    $value = get_option('rocketfuel_merchant_id', '');
    printf('<input type="text" id="rocketfuel_merchant_id" name="rocketfuel_merchant_id" value="%s" />', esc_attr($value));
}

/**
 *  display rocketfuel input in settings form
 */
function rocketfuel_url()
{
    register_setting('general', 'rocketfuel_iframe_url', 'sanitize_text_field');

    add_settings_field(
        'rocketfuel_iframe_url',
        '<label for="rocketfuel_iframe_url">rocketfuel URL</label>',
        'rocketfuel_url_field_html',
        'general'
    );
}

function rocketfuel_url_field_html()
{
    $value = get_option('rocketfuel_iframe_url', '');
    echo '<select id="rocketfuel_iframe_url" name="rocketfuel_iframe_url">';
    foreach (IFRAMES as $option) {
        $selected = ($value == $option) ? 'selected' : '';
        echo '<option value="' . $option . '" ' . $selected . '>' . $option . '</option>';
    }
    echo '</select>';
}


/**
 *  display callback in settings form
 */
function address_for_rocketfuel_callback()
{
    register_setting('general', 'address_for_callback', 'sanitize_text_field');

    add_settings_field(
        'address_for_callback',
        '<label for="address_for_callback">address for rocketfuel callback </label>',
        'address_for_rocketfuel_callback_field_html',
        'general'
    );
}

function address_for_rocketfuel_callback_field_html()
{
    $value = $_SERVER['HTTP_HOST'] . '/wp-json/rocketfuel/payment';
    echo '<p>' . $value . '</p>';
}