<?php
/**
 * Plugin Name: RocketFuel
 * Version: 1.2.4
 * Author: developer
 */

use Automattic\WooCommerce\Admin\Overrides\Order;

if (!defined('ABSPATH')) exit;


if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    exit;
}


define('RF_PLAGIN_DIR', plugin_dir_path(__FILE__));

require_once RF_PLAGIN_DIR . 'includes/rf-overlay_function.php';
require_once RF_PLAGIN_DIR . 'includes/rf-options.php';
//require_once RF_PLAGIN_DIR . 'includes/rf-rocket_curl.php';
//require_once RF_PLAGIN_DIR . 'includes/rf-ajax.php';
require_once RF_PLAGIN_DIR . 'includes/rf-rocketfuel_order.php';
require_once RF_PLAGIN_DIR . 'includes/EncryptAndVerify.php';


add_action('plugins_loaded', 'init_rocketFuel_gateway_class');
add_filter('woocommerce_payment_gateways', 'add_rocketFuel_gateway_class');
add_action('wp_ajax_rocketfuelGetCart', 'ajax_get_cart');
add_action('wp_ajax_nopriv_rocketfuelGetCart', 'ajax_get_cart');

add_action('rest_api_init', function () {
    register_rest_route('rocketfuel', '/payment', [
        'methods' => 'POST',
        'callback' => 'order_callback',
        'args' => array(
            'data' => array(
                'type' => 'string', 'required' => true),
            'signature' => array(
                'type' => 'string', 'required' => true)

        )
    ]);
    register_rest_route('rocketfuel', '/payment', [
        'methods' => 'GET',
        'callback' => 'check_callback',
    ]);
});

function add_rocketFuel_gateway_class($methods)
{
    $methods[] = 'WC_rocketFuel_Gateway';
    return $methods;
}

function init_rocketFuel_gateway_class()
{
    class WC_RocketFuel_Gateway extends WC_Payment_Gateway
    {
        public function __construct()
        {
            $this->id = 'rocketFuel_gateway';
            $this->has_fields = true;
//            $this->icon = dirname(__FILE__) . '/assets/img/icon_gateway.png';
            $this->icon = 'https://www.flaticon.com/svg/static/icons/svg/3587/3587791.svg';
            $this->method_title = 'RocketFuel GateWay';
            $this->method_description = 'RocketFuel GateWay Payment Method';

            //Объязательные методы
            $this->init_form_fields(); //определяем настройки
            $this->init_settings();  //загружаем настройки

            // получаем значение настройки title, доступно после загрузки настроек. Отображаются на странице оформления заказа
            $this->title = $this->get_option('title');
            $this->description = $this->get_option('description');
            $this->merchant_id = get_option('rocketfuel_merchant_id');

            // хук сохранения настроек
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
            add_action('woocommerce_update_options_payment_gateways', array(&$this, 'process_admin_options'));

            //создал callback, который отработает при запросе site/wc-api/rocketFuel_update
            // вызовится метод update_item
            add_action('woocommerce_api_rocketFuel_update', [$this, 'update_item']);

        }

        //Объявляем поля настроек
        public function init_form_fields()
        {
            $this->form_fields = array(
                'enabled' => array(
                    'title' => __('Enable/Disable', 'woocommerce'),
                    'type' => 'checkbox',
                    'label' => __('Enable Cheque Payment', 'woocommerce'),
                    'default' => 'yes'
                ),
                'title' => array(
                    'title' => __('Title', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('This controls the title which the user sees during checkout.', 'woocommerce'),
                    'default' => __('RocketFuel', 'woocommerce'),
                    'desc_tip' => true,
                ),
                'description' => array(
                    'title' => __('Customer Message', 'woocommerce'),
                    'type' => 'textarea',
                    'default' => 'Pay for your order with RocketFuel'
                )
            );
        }

        function admin_options()
        {
            ?>
            <h2><?php _e('You plugin name', 'woocommerce'); ?></h2>
            <table class="form-table">
                <?php $this->generate_settings_html(); ?>
            </table> <?php
        }

        function process_payment($order_id)
        {
            global $woocommerce;
            $order = new WC_Order($order_id);


            // Mark as on-hold (we're awaiting the cheque)
//            $order->update_status('on-hold', __( 'Awaiting cheque payment', 'woocommerce' ));

            // Remove cart
            $woocommerce->cart->empty_cart();

            // Return thankyou redirect
            return array(
                'result' => 'success',
                'redirect' => $this->get_return_url($order)
            );
        }
    }
}

function ajax_get_cart()
{
    try {
        $order = new Rocketfuel_order();
        $send_data = $order->get_order($_POST['order']);
        $send_data['merchant_id'] = get_option('rocketfuel_merchant_id');
        $order_object = EncryptAndVerify::sortPayload($send_data);
        echo json_encode($order_object);

        wp_die();

    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

//function ajax_get_url()
//{
//    $order = new Rocketfuel_order();
//    echo $order->get_order($_POST['orderId'])->get_checkout_order_received_url();
//    wp_die();
//}

function payment_complete($order_id, $transaction_id = '')
{
    if ($order_id == '' || $order_id == null) return;
    $order = new Rocketfuel_order($order_id);
    return $order->order_complete($transaction_id);
}



/**
 * function for POST wp-json/rocketfuel/payment
 *
 * @param WP_REST_Request $request
 * @return array|string
 */
function order_callback(WP_REST_Request $request)
{
    if(!$request->get_param('data')){
        return false;
    }
    $data=json_decode($request->get_param('data'));

    if (!isset($data->offerId) || !$request->get_param('signature')) {
        return false;
    }

    $order = wc_get_order($data->offerId);
    $verify=EncryptAndVerify::verifyPaymentResponse($request, $order->get_id());
    if(!$verify){
        return [
            'status' => 'error',
            'signature not valid'
        ];
    }else{
        $order_id = json_decode($request->get_param('data'))->offerId;
        $status=json_decode($request->get_param('data'))->status;

        if ($status) {
            try {
                $result = try_complete_order(intval($order_id));
            } catch (Exception $e) {
                $result['ok'] = false;
                $result['status'] = 'offer is not found';
            }
            $result['offerId'] = $order_id;
        }

        return $result;
    }


}

/**
 * function for GET wp-json/rocketfuel/payment
 *
 * @return string[]
 */
function check_callback()
{
    return [
        'callback_status' => 'ok'
    ];
}

function try_complete_order($id)
{
    $order = new WC_Order($id);
    if ($order->get_status() == 'processing' || $order->get_status() == 'complete') {
        $result['ok'] = false;
        $result['status'] = 'is processing or complete';
    } else {
        $result['ok'] = $order->payment_complete();
        $result['status'] = $order->get_status();
    }
    return $result;
}

function merchant()
{
    return get_option('merchant_id');
}


