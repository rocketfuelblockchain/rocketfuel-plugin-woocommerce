var extension = false;
var access_token;
var isOverlay = false;
const {
  orderId,
  ajax_url,
  iframe_url
} = rocketFuelOptions || {};
let rfOrder = {};
const iFrameID = 'rocketfuel-iframe';
const draggerID = 'rocketfuel-drag';

async function ajaxSend(data, type = 'POST', url = ajax_url) {
  try {
    let resp = await jQuery.ajax({type, url, data});
    return JSON.parse(resp);
  } catch (e) {
    console.log(e);
    return false;
  }
}

jQuery(document).ready(async function () {
  if(checkExtension()){
    addCartToExtension()
  }
  rfOrder = await ajaxGetCart();
  await openOverLayBlock()
})


async function ajaxRedirect() {
  if (!orderId) return false;
  const url = await jQuery.ajax({type: 'POST', url: ajax_url, data: {action: 'rocketfuelGetURL', orderId}});
  window.location.replace(url);
}

function checkExtension() {
  return typeof rocketfuel === 'object';
}

async function ajaxGetCart() {
  try {
    return ajaxSend(getCartFunctionData());
  } catch (e) {
    return null
  }
}

function getCartFunctionData() {
  let data = {action: "rocketfuelGetCart"}
  if (orderId) data.order = orderId;
  else data.address = getAddressData();
  data.extension = extension;
  return data
}

function getAddressData() {
  let formInputs = {};
  jQuery(`[name=checkout] input[name]`).each(function (ind, itm) {
    let name = itm.name.replace(/^billing_/, '');
    formInputs[name] = itm.value
  });
  return formInputs
}

function createIFrame() {
  let iframe = document.createElement('iframe');
  iframe.title = iFrameID;
  iframe.id = iFrameID;
  iframe.style.display = "none";
  iframe.src = iframe_url;

  iframe.onload = async function () {
    iframe.style.display = 'block';
    createDragger();
    await sendCartToIframe();
  };
  return iframe;
}

function createDragger() {
  const iframe = jQuery(`#${iFrameID}`);
  if (!iframe) return false
  const dragger = jQuery(`<div id="rocketfuel-drag"><div id="rocketfuel-dragheader"></div></div>`);
  iframe.before(dragger)
  dragger.onload = dragElement(dragger, iframe);
}


function dragElement(elmnt, iframe) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    // if present, the header is where you move the DIV from:
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    elmnt.style.position = 'absolute';
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    iframe.style.top = (iframe.offsetTop - pos2) + "px";
    iframe.style.left = (iframe.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

async function openOverLayBlock() {
  if (!isOverlay) {
    if (!checkExtension()) {
      showOverlay(createIFrame())
    } else {
      addCartToExtension()
    }

  } else closeOverlay()

}

function addCartToExtension(data) {
  rocketfuel.setCartData(rfOrder)
  rocketfuel.toggleExtension()
}

function showOverlay(iframe) {
  document.querySelector('body').appendChild(iframe)
  isOverlay = true
}

function closeOverlay() {
  isOverlay = false;
  document.getElementById(iFrameID).remove()
  document.getElementById(draggerID).remove()
}

async function sendCartToIframe() {
  const iframe = document.getElementById(iFrameID);
  if (iframe) {
    // const cart = await ajaxGetCart();
    iframe.contentWindow.postMessage({
      type: 'rocketfuel_send_cart',
      data: rfOrder,
    }, '*');
  }
}

window.addEventListener('message', async (event) => {
  // Это сообщения и для айфрейма и для расширения

  //Это сообщение для айфрейма
  if (event.data.type === 'rocketfuel_new_height') {
    const iframe = document.getElementById(iFrameID);
    if(!!iframe){
      const windowHeight = window.innerHeight - 20;
      if (windowHeight < event.data.data) {
        iframe.style.height = windowHeight+'px';
        iframe.contentWindow.postMessage({
          type: 'rocketfuel_max_height',
          data: windowHeight,
        }, '*');
    }else {
         iframe.style.height = event.data.data+'px';
      }
   }

  }
  if (event.data.type === 'rocketfuel_change_height') {
    document.getElementById(iFrameID).style.height = event.data.data;
  }

  if (event.data.type === 'rocketfuel_get_cart') {
    await sendCartToIframe()
  }
  // Это сообщение для айфрейма
  if (event.data.type === 'rocketfuel_iframe_close') {
    // TODO destroy iframe
    closeOverlay()
  }
  if (event.data.type === 'rocketfuel_result_ok') {
    await ajaxRedirect()
  }
});

